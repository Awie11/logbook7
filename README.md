 Head | Information |
| ------ | ----------- |
| Logbook entry number | 07 |
| Name  | Kee Soon Win |
| Matrix no.  | 195901 |
| Year  | 4 |
| Subsystem  | Simulation |
| Group  | 7 |


## Tables

| Target | Information |
| ------ | ----------- |
| Agenda  | -Run 2D model in Ansys<br> -Guide each other to do the Ansys simulation|
| Goal | -2D HAU model have been run in the simulation<br> -Plot graph for Cl vs angle of attack<br> -To calculate weight of skin,length and surface area|
| Method | -HAU model simulate in Ansys Fluent <br> -Key in the data in excel and plot the graph.<br> -Calculate the Surface area, weight of skin and length by using formula found in the reference book|
| Justification | -During meshing process, skewness and orthogonal quality should take account<br> -Make sure to use the correct formula during the calculation|
| Impacts | -Consider other suitable parameters that are relevant (Lift, Drag, Thrust and Weight)<br> -To get clearer vision of data by plotting the graph|
| Next Steps | -Proceed to get more accurate data for Cl and Cd reading<br> -To get the weight of skin per square metre by looking at the catalogue in lab|

